#!/bin/bash

choice=$(man -k . | dmenu -l 30 | awk '{print $1}')

if [[ $choice = '' ]]
then
	exit 0
else
	man -Tpdf $choice  | okular -
fi

