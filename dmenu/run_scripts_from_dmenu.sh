#!/bin/bash


choice=$(ls -l | grep "\->" | awk '{print $9}' | dmenu -l 23)

if [[ $choice = '' ]]
then
	exit 0
else
	bash $choice
fi
